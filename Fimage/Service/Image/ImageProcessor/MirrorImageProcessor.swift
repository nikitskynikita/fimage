//
//  MirrorImageProcessor.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 29/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

import CoreImage

public struct MirrorImageProcessor: ImageProcessor {
    
    public enum Anchor {
        case xAxis
        case yAxis
    }
    
    private let anchor: Anchor
    
    public init(anchor: Anchor) {
        self.anchor = anchor
    }
    
    public func doProcess(image: CIImage) -> CIImage? {
        let filter = CIFilter(name: "CIAffineTransform")
        filter?.setValue(image, forKey: "inputImage")
        
        let transform: CGAffineTransform
        
        switch anchor {
        case .xAxis: transform = CGAffineTransform(scaleX: -1, y: 1)
        case .yAxis: transform = CGAffineTransform(scaleX: 1, y: -1)
        }
        
        filter?.setValue(transform, forKey: "inputTransform")
        
        return filter?.value(forKey: "outputImage") as? CIImage
    }
    
}
