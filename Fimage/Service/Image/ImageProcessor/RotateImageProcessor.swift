//
//  RotateImageProcessor.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 29/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

import CoreImage

public struct RotateImageProcessor: ImageProcessor {
    
    private let angle: CGFloat
    
    public init(radians angle: CGFloat) {
        self.angle = angle
    }
    
    public func doProcess(image: CIImage) -> CIImage? {
        let filter = CIFilter(name: "CIAffineTransform")
        filter?.setValue(image, forKey: "inputImage")
        
        let transform = CGAffineTransform(rotationAngle: angle)
        filter?.setValue(transform, forKey: "inputTransform")
        
        return filter?.value(forKey: "outputImage") as? CIImage
    }
    
}
