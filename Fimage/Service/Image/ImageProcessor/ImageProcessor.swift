//
//  ImageProcessor.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 29/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

import CoreImage

public protocol ImageProcessor {
    func doProcess(image: CIImage) -> CIImage?
}
