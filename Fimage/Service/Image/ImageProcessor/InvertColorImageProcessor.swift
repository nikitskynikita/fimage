//
//  InvertColorImageProcessor.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 29/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

import CoreImage

public struct InvertColorImageProcessor: ImageProcessor {
    
    public func doProcess(image: CIImage) -> CIImage? {
        let filter = CIFilter(name: "CIPhotoEffectMono")
        filter?.setValue(image, forKey: "inputImage")
        
        return filter?.value(forKey: "outputImage") as? CIImage
    }
    
}
