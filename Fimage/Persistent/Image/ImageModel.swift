//
//  ImageModel.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 30/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

import Foundation

struct ImageModel {
    
    enum State {
        case none
        case loading
        case processing
        case complete(imageData: Data)
        case current(imageData: Data)
    }
    
    var id: Int64 = -1
    var state: State = .none
    
}
