//
//  ImageModelManagedObjectTranslator.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 30/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

class ImageModelManagedObjectTranslator: Translator<DBImageModel, ImageModel> {
    
    override func fill(target: inout DBImageModel, from source: ImageModel) {
        switch source.state {
        case .none, .loading, .processing:
            target.imageData = nil
            target.isCurrent = false
        case let .complete(imageData):
            target.imageData = imageData
            target.isCurrent = false
        case let .current(imageData):
            target.imageData = imageData
            target.isCurrent = true
        }
    }
    
    override func fill(targets: inout [DBImageModel], from sources: [ImageModel]) {
        targets.removeAll()
        
        for source in sources {
            var target = DBImageModel()
            fill(target: &target, from: source)
            targets.append(target)
        }
    }
    
    override func fill(target: inout ImageModel, from source: DBImageModel) {
        target.id = source.id
        
        if let imageData = source.imageData {
            if source.isCurrent {
                target.state = .current(imageData: imageData)
            } else {
                target.state = .complete(imageData: imageData)
            }
        } else {
            target.state = .none
        }
    }
    
    override func fill(targets: inout [ImageModel], from sources: [DBImageModel]) {
        targets.removeAll()
        
        for source in sources {
            var target = ImageModel()
            fill(target: &target, from: source)
            targets.append(target)
        }
    }
    
}
