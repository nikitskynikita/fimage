//
//  Translator.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 30/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

public class Translator<FirstType, SecondType> {
    
    func fill(target: inout FirstType, from source: SecondType) {}
    
    func fill(targets: inout [FirstType], from sources: [SecondType]) {}
    
    func fill(target: inout SecondType, from source: FirstType) {}
    
    func fill(targets: inout [SecondType], from sources: [FirstType]) {}
    
}
