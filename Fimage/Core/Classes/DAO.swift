//
//  DAO.swift
//  Fimage
//
//  Created by Nikita Nikitsky on 30/06/2019.
//  Copyright © 2019 GK CFT. All rights reserved.
//

public class DAO<StoredType> {
    
    func read() -> [StoredType] { return [] }
    
    func read(with id: Int64) -> StoredType? { return nil }
    
    func persist(models: [StoredType]) -> Bool { return false }
    
    func persist(model: StoredType) -> Bool { return false }
    
    func erase() -> Bool { return false }
    
}
